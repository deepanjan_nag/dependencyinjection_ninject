﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DI_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());


            var mailSender = kernel.Get<IMailSender>();
            var formHandler = new FormHandler(mailSender);
            formHandler.Handle("test@test.com");

            Console.ReadLine();
        }
    }

    public interface IMailSender
    {
        void Send(string toAddress, string subject);
    }
    public interface ILogger
    {
        void printer(string message);
    }

    public class MailSender:IMailSender
    {
        private readonly ILogger logger;

        public MailSender(ILogger logger)
        {
            this.logger = logger;
        }

        public void Send(string toAddress, string subject)
        {
            Console.WriteLine("Sending mail to [{0}] with subject [{1}]", toAddress, subject);
            logger.printer("This is the logged message.");
        }
    }

    public class Logger : ILogger
    {
        public void printer(string message)
        {
            Console.Write(message);
        }
    }

    public class FormHandler
    {
        private readonly IMailSender mailSender;

        public FormHandler(IMailSender mailSender)
        {
            this.mailSender = mailSender;
        }

        public void Handle(string toAddress)
        {
            mailSender.Send(toAddress, "This is non-Ninject example");
        }
    }
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IMailSender>().To<MailSender>();
            Bind<ILogger>().To<Logger>();
        }
    }

}
